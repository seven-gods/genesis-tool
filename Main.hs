{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import qualified Data.Bitcoin.Script as BS
import Data.Aeson
import Data.ByteString as B (ByteString, length, unpack)
import Data.ByteString.Lazy.Char8 as L (ByteString, pack, unpack)
import qualified Data.Csv as Csv
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Int
import Data.List
import Data.Maybe
import Data.Scientific (Scientific, FPFormat(Fixed), formatScientific, toBoundedInteger)
import GHC.Generics
import Text.Printf
import System.Environment
import System.IO
import System.Process

getBlackcoindCommand :: IO String
getBlackcoindCommand = do
  customName <- lookupEnv "BLACKCOIND_COMMAND"
  case customName of
    Just name -> return name
    Nothing -> return "blackcoind"

execCommand :: String -> [String] -> IO String
execCommand command args = do
  blackcoindCommand <- getBlackcoindCommand
  let fullCommand = intercalate " " ([blackcoindCommand, command] ++ args)
  readCreateProcess (shell fullCommand) ""

data GenesisEntry = GenesisEntry {
  publicKey :: String,
  balance :: Int64
  } deriving (Generic, Show)
instance ToJSON GenesisEntry

data Block = Block {
  hash :: String,
  height :: Int,
  time :: Int,
  tx :: [String]
  } deriving (Generic, Show)
instance FromJSON Block

data Tx = Tx {
  txid :: String,
  vout :: [VOut]
  } deriving (Generic, Show)
instance FromJSON Tx

data VOut = VOut {
  n :: Int,
  scriptHex :: String,
  value :: Scientific
  } deriving (Show)
instance FromJSON VOut where
  parseJSON (Object o) = do
    n <- o .: "n"
    hex <- ((o .: "scriptPubKey") >>= (.: "hex"))
    value <- o .: "value"
    return (VOut n hex value)

getBlockByNumber :: Int -> IO Block
getBlockByNumber n = do
  -- hPutStr stderr ("\rFetching block " ++ (show n) ++ "…")
  blockJSON <- execCommand "getblockbynumber" [(show n)]
  case decode (pack blockJSON) of
    Just block -> return block

getTxsFromBlock :: Block -> IO [Tx]
getTxsFromBlock block =
  sequence (map getTx (tx block))

getTx :: String -> IO Tx
getTx idx = do
  -- hPutStr stderr ("\rFetching transaction " ++ idx ++ "…")
  txJSON <- execCommand "gettransaction" [idx]
  case decode (pack txJSON) of
    Just tx -> return tx

ourEncodeOptions :: Csv.EncodeOptions
ourEncodeOptions = Csv.defaultEncodeOptions {
  Csv.encQuoting = Csv.QuoteMinimal
  }

blockTxVoutToCSV :: (Block, Tx, VOut) -> L.ByteString
blockTxVoutToCSV (block, tx, vout) =
  Csv.encodeWith ourEncodeOptions [(show (height block),
                                    show (time block),
                                    txid tx,
                                    show (n vout),
                                    scriptHex vout,
                                    formatScientific Fixed (Just 0) (100000000 * (value vout))
                                    )]

first :: (a,b,c) -> a
first (x,_,_) = x

third :: (a,b,c) -> c
third (_,_,z) = z

hex :: B.ByteString -> String
hex = concatMap (printf "%02x") . B.unpack

{-
https://bitcointalk.org/index.php?topic=469640.msg46479723#msg46479723

TXID: 12e8b85da9044fbb5c91c6d37e328eee1758de46eba1d0c808bd78ebecaebaa0
Blackcoin address: BHpmF1MHWZtSJE4YbVomMXHtZWnmoa8URK
Blacknet pubkey: 45be1ee2bae9666d985e5da461a467fcebfbd095a176f72bd5d9ca7620e240ab
Signature: H8+gdveQlxO5sHegDe4zwKRkxo03GATwHyaPnszTmilgft9OXV2UZZ9qKjXo93cvLi/IVzOsxoELCj12W7Ebcs0=
-}
exceptions :: String -> String
exceptions "6a20aeecc9df819291e174ca23ed3a9d75e1032c8fbb1429d107ed24b9c730a09326"   = "6a201cdff8409cf28fd221d6d640c225d53c654f87511016b4abeab0c3cbcf2c3e02"
exceptions "6a21023e30853a282711e70a5d8699bedffa258f80a36febf66fa159341088ddfbd043" = "6a2045be1ee2bae9666d985e5da461a467fcebfbd095a176f72bd5d9ca7620e240ab"
exceptions script = script

getScript :: VOut -> [BS.ScriptOp]
getScript vout =  BS.scriptOps . BS.decode . pack . exceptions . scriptHex $ vout

pubkey :: [BS.ScriptOp] -> String
pubkey (BS.OP_RETURN:BS.OP_PUSHDATA x y:[]) = hex x

toSat :: VOut -> Int64
toSat vout = fromJust $ toBoundedInteger (100000000 * (value vout)) :: Int64

isBlacknetBurnScript :: [BS.ScriptOp] -> Bool
isBlacknetBurnScript (BS.OP_RETURN:BS.OP_PUSHDATA x y:[]) = B.length x == 32
isBlacknetBurnScript _ = False

isBlacknetBurn :: (Block, Tx, VOut) -> Bool
isBlacknetBurn = isBlacknetBurnScript . getScript . third

bonus :: Int -> Double
bonus time = if currWeek <= 4 then 1.2 else 1.2 - (fromIntegral currWeek - 4) * step
  where week = 7*24*60*60
        startTime = 1536278400
        step = 0.02
        currWeek = ceiling $ (fromIntegral (time - startTime)) / (fromIntegral week)

applyBurn :: HashMap String Int64 -> (Block, Tx, VOut) -> HashMap String Int64
applyBurn acc burn = HashMap.insert key (balance + amount) acc
  where key = pubkey . getScript . third $ burn
        balance = fromMaybe 0 $ HashMap.lookup key acc
        amount = floor $ (fromIntegral . toSat . third $ burn) * (bonus . time . first $ burn)

minAmount :: (String, Int64) -> Bool
minAmount acc = snd acc >= 1000000000

processBlocks :: [Int] -> HashMap String Int64 -> IO (HashMap String Int64)
processBlocks [] acc = return acc
processBlocks (height:heights) acc = do
  block <- getBlockByNumber height
  txs <- getTxsFromBlock block
  let txs_vouts = concat [zip3 (repeat block) (repeat tx) (vout tx) | tx <- txs]
  let interesting = filter isBlacknetBurn txs_vouts
  -- mapM_ (putStr . L.unpack . blockTxVoutToCSV) interesting
  processBlocks heights $ foldl' applyBurn acc interesting

main :: IO ()
main = do
  hSetBuffering stdout LineBuffering
  args <- getArgs
  let [start, stop] = map read (take 2 args) :: [Int]
  accounts <- processBlocks [start..stop] HashMap.empty
  let interesting = filter minAmount (HashMap.toList accounts)
  let total = sum . snd . unzip $ interesting
  let supply = 1000000000 * 100000000
  let k = fromIntegral supply / fromIntegral total
  let genesis = map (\(key,balance) -> GenesisEntry key (floor $ k * fromIntegral balance)) interesting
  putStrLn $ L.unpack $ encode genesis
